﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            double num1 = 0;
            double num2 = 0;

            Console.WriteLine("Enter number 1:");
            num1 = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter number 2:");
            num2 = double.Parse(Console.ReadLine());

            BasicCalculations(num1, num2);
        }

        static void BasicCalculations(double num1, double num2)
        {
            Console.WriteLine($"{num1} + {num2} = {num1 + num2}");
            Console.WriteLine($"{num1} - {num2} = {num1 - num2}");
            Console.WriteLine($"{num1} * {num2} = {num1 * num2}");
            Console.WriteLine($"{num1} / {num2} = {num1 / num2}");
        }
    }
}
